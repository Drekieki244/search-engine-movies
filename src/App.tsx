import React, { useState } from "react";
import MoviesList, { MovieDataProps } from "./Components/MoviesList";
import SearchForm from "./Components/SearchForm";

function App() {
  const movies: MovieDataProps[] = [
    { movieName: "iron man", id: "1" },
    { movieName: "iron man 2", id: "2" },
    { movieName: "iron man 3", id: "3" },
    { movieName: "spidy", id: "4" },
  ];
  const [moviesToDisplay, setMoviesToDisplay] =
    useState<MovieDataProps[]>(movies);

  const handleSearch = (searchTerm: string) => {
    const filteredMovies = movies.filter((movie) =>
      movie.movieName.toLowerCase().includes(searchTerm.toLowerCase())
    );
    setMoviesToDisplay(filteredMovies);
  };

  return (
    <div>
      <SearchForm onSearch={handleSearch} />
      <MoviesList movies={moviesToDisplay} />
    </div>
  );
}

export default App;
