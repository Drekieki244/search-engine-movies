import React from "react";
import MovieCard from "../MovieCard";

export interface MovieDataProps {
  movieName: string;
  id: string;
}
export interface MoviesListProps {
  movies: MovieDataProps[];
}
const MoviesList: React.FC<MoviesListProps> = ({ movies }) => {
  return (
    <div>
      {movies.map((movie) => (
        <MovieCard key={movie.id} movieName={movie.movieName} id={movie.id} />
      ))}
    </div>
  );
};

export default MoviesList;
